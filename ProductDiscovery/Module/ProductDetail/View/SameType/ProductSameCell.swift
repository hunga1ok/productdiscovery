//
//  ProductSameCell.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/13/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ProductSameCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    
    
    static let size = CGSize(width: 154, height: 270)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setToSale(false)
    }
    
    func setData(product: Product) {
        if let image = product.images.first {
            let urlRaw = (image.url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            if let url = URL(string: urlRaw) {
                imageView.kf.setImage(with: url)
            }
        }
        
        titleLabel.text = product.name
        guard let sellPrice = product.price?.supplierSalePrice else {
            priceLabel.text = ""
            return
        }
        
        priceLabel.text = Utils.formatCurrency(NSNumber(value: sellPrice))
        
        guard let oldPrice = product.price?.sellPrice else {
            setToSale(false)
            oldLabel.text = ""
            return
        }
        
        let isSale = sellPrice > oldPrice
        setToSale(isSale)
        oldLabel.attributedText = Utils.formatCurrency(NSNumber(value: sellPrice)).strikeThrough()
        
        let percentDiscount = Int(sellPrice * 100/oldPrice - 100)
        percentLabel.text = "\(percentDiscount)%"
        
    }
    
    func setToSale(_ isSale: Bool) {
        oldLabel.isHidden = !isSale
        percentLabel.isHidden = !isSale
    }

}
