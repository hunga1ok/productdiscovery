//
//  APIs.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/9/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import Moya

enum APIs {
    case getListProduct(key: String, page: Int = 1)
    case getProductDetail(sku: String)
}

extension APIs: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://listing.stage.tekoapis.net/api/") else {
            fatalError("baseURL could not be configured")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .getListProduct( _,_):
            return "search"
        case .getProductDetail(let sku):
            return "products/\(sku)"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return "nil".data(using: String.Encoding.utf8) ?? Data()
    }
//    https://listing.stage.tekoapis.net/api/search/?channel=pv_showroom&visitorId=&q=&terminal=CP01
//    https://listing.stage.tekoapis.net/api/products/{product_sku}?channel=pv_showroom&terminal=CP01
    var task: Task {
        switch self {
        case .getListProduct(let key, let page):
            return .requestParameters(parameters: ["channel": "pv_online", "visitorId": "", "q": key, "terminal": "CP01", "_page": "\(page)", "_limit": "10"], encoding: URLEncoding.queryString)
        case .getProductDetail( _):
            return .requestParameters(parameters: ["channel": "pv_online", "terminal": "CP01"], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
    
}
