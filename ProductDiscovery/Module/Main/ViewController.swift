//
//  ViewController.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/9/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onGetStartedBTPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "ListProduct", bundle: .main).instantiateViewController(withIdentifier: "ListProductViewController") as! ListProductViewController
        let vm = ListProductViewModel()
        vc.viewModel = vm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

