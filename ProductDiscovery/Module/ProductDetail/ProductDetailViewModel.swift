//
//  ProductDetailViewModel.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/11/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
struct ProductDetailViewModel {
    var productData: Product
    var sameTypeProducts: [Product]
    
    init(product: Product) {
        productData = product
        sameTypeProducts = []
    }
}
