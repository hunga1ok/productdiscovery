//
//  ProductSaleInfoCell.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/11/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ProductSaleInfoCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeProductLabel: UILabel!
    @IBOutlet weak var outOfStockLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var outOfStockHeight: NSLayoutConstraint!
    
    static let cellHeight: CGFloat = 466.0
    var numberOfItem: Int = 3
    var images: [Image] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    
    func setupData(_ product: Product) {
        images = product.images.sorted { (image1, image2) -> Bool in
            return image1.priority < image2.priority
        }
        numberOfItem = images.count
        setupPageControl()
        self.titleLabel.text = product.name
        self.codeProductLabel.text = product.sku
        setToSale(false)
        guard let salePrice = product.price?.supplierSalePrice else {
            self.priceLabel.text = ""
            return
        }
        
        self.priceLabel.text = Utils.formatCurrency(NSNumber(value: salePrice))
        
        guard let oldPrice = product.price?.sellPrice else {
            return
        }
        let isSale = oldPrice > salePrice
        setToSale(isSale)
        self.oldPriceLabel.attributedText = Utils.formatCurrency(NSNumber(value: oldPrice)).strikeThrough()
        let percentDiscount = Int((salePrice*100/oldPrice) - 100)
        self.discountLabel.text = "\(percentDiscount) %"
        self.outOfStockLabel.isHidden = product.totalAvailable != nil
        outOfStockHeight.constant = outOfStockLabel.isHidden ? 0 : 22
    }
    
    func setToSale(_ isSale: Bool) {
        self.oldPriceLabel.isHidden = !isSale
        self.discountLabel.isHidden = !isSale
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            collectionView.collectionViewLayout = layout
        }
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.indicatorStyle = .white
    }
    
    func setupPageControl() {
        pageControl.numberOfPages = numberOfItem
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(self.collectionView.contentOffset.x)/Int(self.collectionView.frame.width)
    }
    
}

extension ProductSaleInfoCell: UICollectionViewDelegate {

}

extension ProductSaleInfoCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: 300)
    }
}

extension ProductSaleInfoCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return numberOfItem
        return numberOfItem > 0 ? numberOfItem : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell else {
            return UICollectionViewCell()
        }
        if numberOfItem == 0 {
            return cell
        }
        let urlRaw = images[indexPath.item].url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? images[indexPath.item].url
        print("loadImage: urlRaw = \(urlRaw)")
        let url = URL(string: urlRaw)
        cell.imageView.kf.setImage(with: url)
        return cell
    }
}
