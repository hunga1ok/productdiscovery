//
//  Ultils.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import UIKit

struct Utils {
    static func gradientColor(colorTop: UIColor, colorBottom: UIColor) -> CAGradientLayer {
        let gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        return gl
    }
    static func formatCurrency(_ inputNumber: NSNumber, symbol: String = "đ") -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        currencyFormatter.currencySymbol = symbol
        currencyFormatter.currencyGroupingSeparator = "."
        currencyFormatter.numberStyle = .currency
        currencyFormatter.positiveFormat = "#,##0 ¤"
        return currencyFormatter.string(from: inputNumber) ?? "\(inputNumber)"
    }
}
