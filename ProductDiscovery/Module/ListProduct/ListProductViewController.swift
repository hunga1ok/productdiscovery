//
//  ListProductViewController.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/9/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit
import Kingfisher

protocol ListProductViewControllerDelegate: NSObjectProtocol {
    func reloadData()
}

class ListProductViewController: UITableViewController {

    var viewModel: ListProductViewModel!
    let searchBar = UISearchBar()
    private lazy var backButton: UIBarButtonItem = {
            var image = UIImage(named: "arrowBack")
            image = image?.imageFlippedForRightToLeftLayoutDirection()
            return UIBarButtonItem(image: image,
                                          style: .plain,
                                          target: self,
                                          action: #selector(onPressedBackButton))
        }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getListProduct(with: "")
        viewModel.delegate = self
        
        tableView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: ProductCell.id)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
        searchBar.text = viewModel.currentSearchText
    }
    
    private func setupNavigation() {
        self.navigationController?.navigationBar.backgroundColor = UIColor.AppColor.reddishOrange
        searchBar.sizeToFit()
        searchBar.placeholder = "Nhập tên, mã sản phẩm"
        definesPresentationContext = true
        searchBar.delegate = self
        searchBar.searchTextField.backgroundColor = .white
        
        navigationItem.titleView = searchBar
        
        navigationItem.leftBarButtonItem = backButton
        navigationController?.navigationBar.barTintColor = UIColor.AppColor.reddishOrange
        navigationController?.navigationBar.tintColor = .white
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.listProducts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductCell.id, for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        let product = viewModel.listProducts[indexPath.row]
        cell.titleLabel.text = product.name
        if !product.images.isEmpty {
            let urlRaw = product.images[0].url
            print("load link \(urlRaw)")
            let url = URL(string: urlRaw.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            print("url \(String(describing: url))")
            cell.imgView.kf.setImage(with: url)
        } else {
            cell.imgView.image = UIImage(named: "thumbnail")
        }
        
        
        guard let supplierSalePrice = product.price?.supplierSalePrice else {
            cell.setToSale(false)
            return cell
        }
        cell.priceLabel.text = Utils.formatCurrency(NSNumber(value: supplierSalePrice))
        
        guard let sellPrice = product.price?.sellPrice else {
            cell.setToSale(false)
            return cell
        }
        
        let isSale = sellPrice > supplierSalePrice
        cell.setToSale(isSale)
        
        cell.oldPriceLabel.attributedText = "\(sellPrice.formattedWithSeperator)".strikeThrough()
        
        let percentDiscount = Int((supplierSalePrice*100/sellPrice) - 100)
        cell.discountLabel.text = "\(percentDiscount) %"
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ProductCell.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "ProductDetail", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        var vm = ProductDetailViewModel(product: self.viewModel.listProducts[indexPath.row])
        vm.sameTypeProducts = viewModel.listProducts
        vc.viewModel = vm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func onPressedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.searchTextField.endEditing(true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.viewModel.listProducts.count - 1
        if indexPath.row == lastElement {
            self.viewModel.loadMore()
        }
    }

}

extension ListProductViewController: ListProductViewControllerDelegate {
    func reloadData() {
        tableView.reloadData()
    }
}



extension ListProductViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        let searchText = searchBar.text ?? ""
        if searchText != viewModel.currentSearchText {
            tableView.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
            viewModel.getListProduct(with: searchText)
        }
        

    }
}
