//
//  Price.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation

struct Price {
    let supplierSalePrice: Double?
    let sellPrice: Double?
}

extension Price: Decodable {
    
    enum PriceCodingKeys: String, CodingKey {
        case supplierSalePrice = "supplierSalePrice"
        case sellPrice = "sellPrice"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PriceCodingKeys.self)
        supplierSalePrice = try container.decodeIfPresent(Double.self, forKey: .supplierSalePrice)
        sellPrice = try container.decodeIfPresent(Double.self, forKey: .sellPrice)
    }
}

struct Image {
    let url: String
    let priority: Int
    let path: String
}

extension Image: Decodable {
    enum ImageCodingKeys: String, CodingKey {
        case url = "url"
        case priority = "priority"
        case path = "path"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ImageCodingKeys.self)
        url = try container.decode(String.self, forKey: .url)
        priority = try container.decode(Int.self, forKey: .priority)
        path = try container.decode(String.self, forKey: .path)
    }
}

struct SeoInfo {
    let metaKeyword: String?
    let metaTitle: String?
    let metaDescription: String?
    let shortDescription: String?
    let description: String?
}

extension SeoInfo: Decodable {
    enum SeoCodingKeys: String, CodingKey {
        case metaKeyword = "metaKeyword"
        case metaTitle = "metaTitle"
        case metaDescription = "metaDescription"
        case shortDescription = "shortDescription"
        case description = "description"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SeoCodingKeys.self)
        metaKeyword = try container.decodeIfPresent(String.self, forKey: .metaKeyword)
        metaTitle = try container.decodeIfPresent(String.self, forKey: .metaTitle)
        metaDescription = try container.decodeIfPresent(String.self, forKey: .metaDescription)
        shortDescription = try container.decodeIfPresent(String.self, forKey: .shortDescription)
        description = try container.decodeIfPresent(String.self, forKey: .description)
    }
}
