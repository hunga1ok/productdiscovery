//
//  ResultResponse.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import Moya

struct ResultResponse {
    let result: ProductResults
}

extension ResultResponse: Decodable {
    
    private enum ResultsCodingKeys: String, CodingKey {
        case result = "result"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ResultsCodingKeys.self)
        
        result = try container.decode(ProductResults.self, forKey: .result)
    }
}
