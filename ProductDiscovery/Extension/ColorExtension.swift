//
//  ColorExtension.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct AppColor {
        static let primary = UIColor(red: 223/255, green: 32/255, blue: 32/255, alpha: 1.0)
        static let darkGrey = UIColor(red: 38/255, green: 40/255, blue: 41/255, alpha: 1.0)
        static let coolGrey = UIColor(red: 143/255, green: 149/255, blue: 152/255, alpha: 1.0)
        static let reddishOrange = UIColor(red: 245/255, green: 71/255, blue: 30/255, alpha: 1.0)
    }   
    
}
