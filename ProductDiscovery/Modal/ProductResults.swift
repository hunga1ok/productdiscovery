//
//  ProductResults.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import Moya

struct ProductResults {
    let products: [Product]
}

extension ProductResults: Decodable {
    enum MovieCodingKeys: String, CodingKey {
        case products = "products"
    }
    
    init(from decoder: Decoder) throws {
        let contaner = try decoder.container(keyedBy: MovieCodingKeys.self)
        products = try contaner.decode([Product].self, forKey: .products)
    }
}
