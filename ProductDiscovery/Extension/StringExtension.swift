//
//  StringExtension.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Calculate size of box that the string required to draw
extension String {
    func height(withConstraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        guard !self.isEmpty else { return .zero }
        
        let boundingBoxSize = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: boundingBoxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        guard !self.isEmpty else { return .zero }
        
        let boundingBoxSize = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boudingBox = self.boundingRect(with: boundingBoxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boudingBox.width)
    }
    
    func widthOfString(withFont font: UIFont) -> CGFloat {
        guard !self.isEmpty else { return 0.0 }
        
        let fontAttribute = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttribute)
        
        return ceil(size.width)
    }
    
    func heightOfString(withFont font: UIFont) -> CGFloat {
        guard !self.isEmpty else { return 0.0 }
        
        let fontAttribute = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttribute)
        
        return ceil(size.height)
    }
}

// MARK: - More
extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
