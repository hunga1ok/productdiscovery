//
//  Product.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/9/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import UIKit

struct Product {
    let sku: String?
    let name: String?
    let url: String?
    let price: Price?
    let images: [Image]
    let totalAvailable: Double?
    let seoInfo: SeoInfo
    
}

extension Product: Decodable {
    enum ProductCodingKeys: String, CodingKey {
        case sku = "sku"
        case name = "name"
        case url = "url"
        case price = "price"
        case images = "images"
        case totalAvailable = "totalAvailable"
        case seoInfo = "seoInfo"
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ProductCodingKeys.self)
        sku = try container.decode(String.self, forKey: .sku)
        name = try container.decode(String.self, forKey: .name)
        url = try container.decode(String.self, forKey: .url)
        price = try container.decode(Price.self, forKey: .price)
        images = try container.decode([Image].self, forKey: .images)
        totalAvailable = try container.decodeIfPresent(Double.self, forKey: .totalAvailable)
        seoInfo = try container.decode(SeoInfo.self, forKey: .seoInfo)
    }
}
