//
//  ImageCell.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/11/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
