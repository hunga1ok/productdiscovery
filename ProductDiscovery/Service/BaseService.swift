//
//  BaseService.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/9/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON

protocol Networkable {
    var provider: MoyaProvider<APIs> { get }
    func getListProduct(key: String, page: Int, completion: @escaping ([Product])->())
}

struct NetworkManager: Networkable {
    var provider = MoyaProvider<APIs>()
    
    func getListProduct(key: String, page: Int, completion: @escaping ([Product]) -> ()) {
        provider.request(.getListProduct(key: key, page: page), callbackQueue: nil, progress: nil) { (result) in
            switch result {
            case let .success(moyaResponse):
//                let statusCode = moyaResponse.statusCode
                let data = moyaResponse.data
                do {
                    let results = try JSONDecoder().decode(ResultResponse.self, from: data)
                    let products = results.result.products
                    completion(products)

                } catch let parseError as NSError {
                    print("JSON Error \(parseError.localizedDescription)")
                }

                break
            case let .failure(error):
                print("hung.lv2 get request failure with error \(error)")
            }
        }
    }

}
