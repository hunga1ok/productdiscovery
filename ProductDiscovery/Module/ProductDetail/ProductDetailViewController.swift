//
//  ProductDetailViewController.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/11/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    // MARK: - Outlet and Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numberOfProductLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var totalPriceContainerView: UIView!
    
    var viewModel: ProductDetailViewModel!
    var productMoreInfoCell: ProductMoreInfoCell!
    private lazy var backButton: UIBarButtonItem = {
        var image = UIImage(named: "arrowBack")
        image = image?.imageFlippedForRightToLeftLayoutDirection()
        return UIBarButtonItem(image: image,
                                      style: .plain,
                                      target: self,
                                      action: #selector(onPressedBackButton))
    }()
    
    private lazy var cartButton: UIBarButtonItem = {
        var image = UIImage(named: "cartOutlined")
        image = image?.imageFlippedForRightToLeftLayoutDirection()
        return UIBarButtonItem(image: image,
                                      style: .plain,
                                      target: self,
                                      action: nil)
    }()
    
    var numberOfProduct = 0 {
        didSet {
            onChangePrice()
        }
    }
    
    
    // MARK: - Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupTableView()
        setupSubView()
    }
    
    // MARK: - Methods
    func setupNavigation() {
        let headerView = Bundle.main.loadNibNamed("ProductDetailHeader", owner: self, options: nil)?[0] as! ProductDetailHeaderView
        headerView.titleLabel.text = viewModel.productData.name
        if let supplierSalePrice = viewModel.productData.price?.supplierSalePrice {
            headerView.priceLabel.text = Utils.formatCurrency(NSNumber(value: supplierSalePrice))
        } else {
            headerView.priceLabel.text = ""
        }
        headerView.sizeToFit()
        navigationItem.titleView = headerView
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = .black
        navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItem = cartButton
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProductSaleInfoCell", bundle: nil), forCellReuseIdentifier: "ProductSaleInfoCell")
        tableView.register(UINib(nibName: "ProductMoreInfoCell", bundle: nil), forCellReuseIdentifier: "ProductMoreInfoCell")
        tableView.register(UINib(nibName: "ProductSameTypeCell", bundle: nil), forCellReuseIdentifier: "ProductSameTypeCell")
        
        productMoreInfoCell = (tableView.dequeueReusableCell(withIdentifier: "ProductMoreInfoCell") as! ProductMoreInfoCell)
    }
    
    func setupSubView() {
        totalPriceContainerView.layer.cornerRadius = 10
        totalPriceContainerView.clipsToBounds = true
    }
    
    func showWarning() {
        let alert = UIAlertController(title: "Thông báo", message: "Hiện không còn hàng để thêm!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func onChangePrice() {
        guard let price = viewModel.productData.price?.supplierSalePrice else {
            return
        }
        numberOfProductLabel.text = "\(numberOfProduct)"
        totalPriceLabel.text = Utils.formatCurrency(NSNumber(value: price * Double(numberOfProduct)))
    }
    
    @objc func onPressedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onPressReduceProduct(_ sender: Any) {
        if numberOfProduct > 0 {
            numberOfProduct -= 1
        }
    }
    
    @IBAction func onPressAddProduct(_ sender: Any) {
        if let totalAvaiable = viewModel.productData.totalAvailable, totalAvaiable > 0, numberOfProduct < Int(totalAvaiable) {
            numberOfProduct += 1
        } else {
            showWarning()
        }
    }
    

}

// MARK: - UITableViewDelegate
extension ProductDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return ProductSaleInfoCell.cellHeight
        case 1:
            return productMoreInfoCell.cellHeightExplain
        case 2:
            return ProductSameTypeCell.cellHeight
        default:
            return 1.1
        }
    }
}

// MARK: - UITableViewDataSource
extension ProductDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductSaleInfoCell", for: indexPath) as? ProductSaleInfoCell else {
                return UITableViewCell()
            }
            cell.setupData(self.viewModel.productData)
            return cell
        case 1:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductMoreInfoCell", for: indexPath) as? ProductMoreInfoCell else {
//                return UITableViewCell()
//            }
//            cell.setupData(viewModel.productData)
            productMoreInfoCell.setupData(viewModel.productData)
            return productMoreInfoCell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductSameTypeCell", for: indexPath) as? ProductSameTypeCell else {
                return UITableViewCell()
            }
            cell.productsData = self.viewModel.sameTypeProducts
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
}
