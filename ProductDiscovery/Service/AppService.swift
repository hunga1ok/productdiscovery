//
//  AppService.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation

struct AppService {
    public static let shared = AppService()
    
    var networkManager: NetworkManager
    
    private init() {
        networkManager = NetworkManager()
    }
}
