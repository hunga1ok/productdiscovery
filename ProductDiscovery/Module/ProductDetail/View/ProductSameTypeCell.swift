//
//  ProductSameTypeCell.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/13/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ProductSameTypeCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    static let cellHeight: CGFloat = ProductSameCell.size.height + 30
    var productsData: [Product] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func gotoViewController(vc: UIViewController) {
        guard let parentVC = self.parentViewController else {
            return
        }
        
        parentVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ProductSameCell", bundle: nil), forCellWithReuseIdentifier: "ProductSameCell")
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
            collectionView.collectionViewLayout = layout
        }
    }
}

extension ProductSameTypeCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "ProductDetail", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        var vm = ProductDetailViewModel(product: productsData[indexPath.row])
        vm.sameTypeProducts = productsData
        vc.viewModel = vm
        self.gotoViewController(vc: vc)
    }
}

extension ProductSameTypeCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductSameCell", for: indexPath) as? ProductSameCell else {
            return UICollectionViewCell()
        }
        cell.setData(product: productsData[indexPath.row])
        return cell
    }
}

extension ProductSameTypeCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ProductSameCell.size
    }
}
