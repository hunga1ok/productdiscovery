//
//  ListProductViewModel.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/9/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation
import UIKit

class ListProductViewModel: NSObject {
    
    weak var delegate: ListProductViewControllerDelegate?
    
    var listProducts: [Product] = []
    var currentSearchText: String = ""
    var currentPage = 0
    
    
    func getListProduct(with key: String) {
        if (currentSearchText != key) {
            currentPage = 0
        }
        currentSearchText = key
        currentPage += 1
        AppService.shared.networkManager.getListProduct(key: key, page: currentPage) { [weak self] (listProduct) in
            guard let `self` = self else { return }
            print("load done: \(listProduct.count)")
            if self.currentPage > 1 {
                self.listProducts.append(contentsOf: listProduct)
            } else {
                self.listProducts = listProduct
            }
            self.delegate?.reloadData()
        }
    }
    
    func loadMore() {
        getListProduct(with: currentSearchText)
    }
}
