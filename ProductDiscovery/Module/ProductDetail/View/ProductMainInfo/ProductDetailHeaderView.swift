//
//  ProductDetailHeaderView.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/11/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ProductDetailHeaderView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    static let height: CGFloat = 57
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
