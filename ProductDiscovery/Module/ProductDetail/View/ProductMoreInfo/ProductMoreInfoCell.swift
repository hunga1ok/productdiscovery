//
//  ProductMoreInfoCell.swift
//  ProductDiscovery
//
//  Created by Le Hung on 3/13/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import UIKit

class ProductMoreInfoCell: UITableViewCell {

    @IBOutlet weak var menuBarView: MenuTabsView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerExplainView: UIView!
    @IBOutlet weak var explainButton: UIButton!
    
    static let cellHeight: CGFloat = 200
    var cellHeightExplain: CGFloat = ProductMoreInfoCell.cellHeight
    var descriptionData: [(String, String)] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupMenuBarView()
        setupTableView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMenuBarView() {
        menuBarView.dataArray = ["Mô tả sản phẩm", "Thông số kỹ thuật", "So sánh giá"]
        menuBarView.isSizeToFitCellsNeeded = true
        menuBarView.collView.backgroundColor = UIColor.init(white: 0.97, alpha: 0.97)
        menuBarView.menuDelegate = self
        //For Intial Display
        if menuBarView.dataArray.count > 0 {
            menuBarView.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        }
    }
    
    func setupData(_ product: Product) {
        guard let description = product.seoInfo.shortDescription else {
            return
        }
        
        let replaced = description.replacingOccurrences(of: "<br/>", with: "|").replacingOccurrences(of: "<br>", with: "|")
        
        let listSub = replaced.split(separator: "|")
        for sub in listSub {
            let sub2 = sub.split(separator: ":", maxSplits: 2)
            if sub2.count > 1 {
                descriptionData.append((String(sub2[0]), String(sub2[1])))
            }
        }
        let isShowExplainView =  CGFloat(40 + 60 * (descriptionData.count)) > ProductMoreInfoCell.cellHeight
        showExplainView(isShow: isShowExplainView)
        tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "InfoCell", bundle: nil), forCellReuseIdentifier: "InfoCell")
        tableView.isScrollEnabled = false
    }
    
    
    @IBAction func onExplainButtonPressed(_ sender: Any) {
        cellHeightExplain = descriptionData.count > 0 ? CGFloat(40 + 60 * (descriptionData.count)) : ProductMoreInfoCell.cellHeight
        guard let vc = self.parentViewController as? ProductDetailViewController else { return }
        vc.tableView.beginUpdates()
        vc.tableView.endUpdates()
        showExplainView(isShow: false)
    }
    
    func showExplainView(isShow: Bool) {
        containerExplainView.isHidden = !isShow
        explainButton.isHidden = !isShow
    }
    
}

extension ProductMoreInfoCell: MenuBarDelegate {
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        return
    }
}

extension ProductMoreInfoCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        descriptionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as? InfoCell else {
            return UITableViewCell()
        }
        
        cell.titleLabel.text = descriptionData[indexPath.row].0
        print(descriptionData[indexPath.row].0)
        cell.descriptionLabel.text = descriptionData[indexPath.row].1
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}
