//
//  NumberExtension.swift
//  ProductDiscovery
//
//  Created by LeHung on 3/10/20.
//  Copyright © 2020 LeHung. All rights reserved.
//

import Foundation

extension Formatter {
    static let withSeperator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
    
    static let readableFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        return formatter
    }()
    
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.current
        
        return dateFormatter
    }()
}
extension Int64 {
    var formattedWithSeperator: String {
        return Formatter.withSeperator.string(for: self) ?? ""
    }
}

extension Double {
    var formattedWithSeperator: String {
        return Formatter.withSeperator.string(for: self) ?? ""
    }
}

extension Int {
    var formattedWithSeperator: String {
        return Formatter.withSeperator.string(for: self) ?? ""
    }
}

extension Float {
    var cleanZeroRedundant: String {
        return String(format: "%g", self)
    }
    
    var readableNumber: NSNumber {
        return Formatter.readableFormatter.number(from: self.cleanZeroRedundant) ?? 0
    }
}
extension NSNumber {
    var formattedWithSeperator: String {
        return Formatter.withSeperator.string(for: self) ?? ""
    }
}
